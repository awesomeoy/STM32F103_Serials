# STM32F103_Serials


						基于STM32芯片设计的简易串口框架
	说明:
	1、串口接收开启DMA并结合空闲中断获取不定长数据,串口数据通过DMA输出数据
	2、框架基于STM32F103+Cubemx库进行的调试，目前只支持三路串口和usb虚拟串口，如果想扩展串口，
		 简单添加部分代码即可
	3、框架覆盖Cubemx库中断调用的串口函数，如下:
		 void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
		 void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
		 void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
		 如果使用usb虚拟串口，需要将函数
		 void usb_receive_callback(uint8_t* data, uint32_t len)放到usb数据接收位置。
	 4、Cubemx串口库需要局部进行修改:
		 HAL_UART_Receive_DMA()和HAL_UART_Transmit_DMA()函数中将DMA传输半中断调用函数赋值为NULL
		 huart->hdmarx->XferHalfCpltCallback = NULL;
		 
	 使用说明:(框架源码在serials目录下)
	 1、使能打开硬件对应串口模块，宏定义选择打开，例如开启串口1，修改为 #define USART1_ENABLE 1
	 2、必须调用serial_init()函数对串口资源进行初始化，并开启串口接收数据
	 3、用户上层代码选择对serial_process_callback()函数进行覆盖定义，需循环调用该函数对串口数据进行收发处理
	 4、用户上层可调用serial_write()向串口发送缓冲区中写入数据，需指定串口ID
	 5、用户上层可调用serial_read()读取串口接收缓冲区中数据，需指定串口ID

