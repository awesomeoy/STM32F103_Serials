
#ifndef SERIAL_H
#define SERIAL_H
	#include "stm32f1xx_hal.h"
	#include "AwesomeUsart.h"
		
	#define USART_MAX_NUM 4
	
	#define USART1_ENABLE 1
	#define USART2_ENABLE 1
	#define USART3_ENABLE 1
	#define USB_USART_ENABLE 0
	
	#if ( USB_USART_ENABLE==1 )
		#include "usbd_cdc_if.h"
	#endif
	
	/// 串口虚拟ID号
	typedef enum {
		SERIAL_ID_NONE = 0,
		SERIAL_ID_1,     /// 对应串口1
		SERIAL_ID_2,		 /// 对应串口2
		SERIAL_ID_3,		 /// 对应串口3
		SERIAL_ID_4,
		SERIAL_ID_5,
		SERIAL_ID_6,
		SERIAL_ID_7,
		SERIAL_ID_USB
	}SERIAL_ID;
	
	/// 串口发送标志
	typedef enum {
		SERIAL_SEND_STATUS_NONE = 0,
		SERIAL_SEND_STATUS_SUCESS,     /// 发送成功
		SERIAL_SEND_STATUS_SENDING		 /// 发送中
	}SERIAL_SEND_STATUS;
	
	typedef __packed struct{
		SERIAL_ID id;
		__IO SERIAL_SEND_STATUS send_status : 3;
		__IO uint8_t receive_status : 1;
		void* huart;
		awesome_usart* pawesome_usart;
		uint8_t* ptx;
		uint8_t* prx;
		uint8_t* pdmatx;
		uint8_t* pdmarx;
		uint32_t tx_max_size;
		uint32_t rx_max_size;
		uint32_t tx_max_dma_size;
		uint32_t rx_max_dma_size;
	}Serial;
	
	void serial_init(void);
	HAL_StatusTypeDef open_serial_receive(Serial* ps);
	HAL_StatusTypeDef open_usart_send(Serial* ps);
	void serial_write(SERIAL_ID id, uint8_t* data, uint32_t len);
	uint32_t serial_read(SERIAL_ID id, uint8_t* data, uint32_t len);
	Serial* get_serial_by_id(SERIAL_ID id);
	Serial* get_serial(void* huart);
	void usb_receive_callback(uint8_t* data, uint32_t len);
	
	__weak void serial_process_callback(void);
	
	extern Serial* serials[USART_MAX_NUM];
	
#endif
