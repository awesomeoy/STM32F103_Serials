#ifndef USER_H
#define USER_H
	#include "stm32f1xx_hal.h"
	
	#define ON		GPIO_PIN_RESET
	#define OFF		GPIO_PIN_SET
	
	#define led(x) { if(x==OFF){ HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, OFF); }else{ HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, ON); } }
	
	void system_init(void);
	
	void app(void);
	
	void led_task(void);
		
#endif

